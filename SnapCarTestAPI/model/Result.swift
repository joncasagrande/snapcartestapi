//
//  Result.swift
//  SnapCarTestAPI
//
//  Created by Jonathan on 30/08/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
// https://app.quicktype.io

import Foundation
// MARK: - Result
struct Result: Codable {
    let ci: String?
    let distance: Double?
    let user: User
    let priceInformation: PriceInformation
    let car: Car
    let flags: Flags
}
