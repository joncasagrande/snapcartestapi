//
//  File.swift
//  SnapCarTestAPITests
//
//  Created by Jonathan on 31/08/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
//

import XCTest
@testable import SnapCarTestAPI

class PriceInformationTest: XCTestCase {
    
    var priceInformation :PriceInformation?
    let jsonPrice = Data("""
{
                "price": 30,
                "pricePerKilometer": 0.25,
                "freeKilometersPerDay": 100,
                "rentalDays": 1,
                "isoCurrencyCode": "EUR"
            }
""".utf8)
    
    override func setUp() {
        priceInformation = try? JSONDecoder().decode(PriceInformation.self, from: jsonPrice)
    }
    
    override func tearDown() {
        priceInformation = nil
    }
    
    func testNotNil() {
        XCTAssertNotNil(priceInformation)
    }
    
    func testPerformanceExample() {
        XCTAssertEqual(priceInformation?.price, 30)
    }
    
}

