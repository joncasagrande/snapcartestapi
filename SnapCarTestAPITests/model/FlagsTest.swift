//
//  FlagsTest.swift
//  SnapCarTestAPITests
//
//  Created by Jonathan on 31/08/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
//

import Foundation
import XCTest

@testable import SnapCarTestAPI

class FlagTests: XCTestCase {
    var flags : Flags?
    let jsonFlag = Data("""
        {
                "favorite": true,
                "new": false,
                "instantBookable": false,
                "previouslyRented": false,
                "isKeyless": false
            }
""".utf8)
    
    override func setUp() {
        let flags1 = try? JSONDecoder().decode(Flags.self, from: jsonFlag) as Flags
        flags = flags1
    }
    
    override func tearDown() {
        flags = nil
    }
    
    func testParseJson() {
        XCTAssertNotNil(flags)
        
    }
    func testReview() {
        XCTAssertTrue(flags?.favorite == true)
    }
    
}
