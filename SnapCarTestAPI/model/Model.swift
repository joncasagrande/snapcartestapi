//
//  Model.swift
//  SnapCarTestAPI
//
//  Created by Jonathan on 01/09/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
//

import Foundation
enum Model: Codable {
    case integer(Int)
    case string(String)
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(Model.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Model"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
    
    func description() -> String {
        switch self {
            case .integer(let x):
                return String(x)
            case .string(let x):
                return x
        }
    }
}


