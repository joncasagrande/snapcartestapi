//
//  SnapsTableViewCell.swift
//  SnapCarTestAPI
//
//  Created by Jonathan on 30/08/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
//

import UIKit
import Alamofire

class SnapsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageCar: UIImageView!
    @IBOutlet weak var carNameText: UILabel!
    @IBOutlet weak var fuelTypeText: UILabel!
    @IBOutlet weak var priceText: UILabel!
    @IBOutlet weak var costPerText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func bind(result: Result) {
        //if(result.car.images != nil && !result.car.images!.isEmpty){
            //result.car.images![0]
            downloadImage(from: URL(string: "https://i.ytimg.com/vi/EONVXpMvAmc/maxresdefault.jpg")!)
        //}

        carNameText.text = result.car.model.description()
        fuelTypeText.text = result.car.fuelType
        priceText.text = result.priceInformation.price!.description
        costPerText.text = ""
    }
    //Not good but solved to be fast
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    func downloadImage(from url: URL) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                self.imageCar.image = UIImage(data: data)
            }
        }
    }
}
