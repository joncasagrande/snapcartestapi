//
//  ViewController.swift
//  SnapCarTestAPI
//
//  Created by Jonathan on 28/08/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class ViewController: UIViewController {
    
    @IBOutlet weak var countryText: UITextField!
   
    @IBOutlet weak var tableViewSnaps: UITableView!
    @IBOutlet weak var orderByLabl: UILabel!
    @IBOutlet weak var LimitSlider: UISlider!
    @IBOutlet weak var OffSetSlider: UISlider!
   
    @IBOutlet weak var orderByButtonText: UIButton!
    @IBOutlet weak var RecomendeSort: UIButton!
    @IBOutlet weak var noResultText: UILabel!
    
    var viewModel : RequestViewModel = RequestViewModel()
    
    var sortBy :String?
    var orderBy : String?
    
    var query = QueryParams()
    var locationManager: CLLocationManager!
    var lat = 0.0
    var lng = 0.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        
        LimitSlider.maximumValue = 10
        LimitSlider.minimumValue = 0
    
        OffSetSlider.minimumValue = 0
        OffSetSlider.maximumValue = 1000
        
        viewModel.requestSnaps(query: QueryParams())
        viewModel.delegate = self
        
        setupUI()
        locationManager.requestAlwaysAuthorization()
        loadCurrentLocation()
    }
    
    func loadCurrentLocation() {
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
    }
    
    
    func setupUI() {
        tableViewSnaps.delegate = self
        tableViewSnaps.dataSource = self
    }
    
    @IBAction func sortByRecomendeClick(_ sender: Any) {
        sortBy = "recommended"
    }
    
    
    @IBAction func sortByPriceClick(_ sender: Any) {
        sortBy = "price"
    }
    
    @IBAction func sortByDistanceClick(_ sender: Any) {
        sortBy = "distance"
    }
    
    @IBAction func OrderBy(_ sender: Any) {
        if(orderBy == nil || orderBy == "Asc"){
            
            orderByButtonText.setTitle("Desc", for: .normal)
            orderBy = "Desc"
        }else{
            orderByButtonText.setTitle("Asc", for: .normal)
            orderBy = "Asc"
        }
        
    }
    
    @IBAction func CurrentLocationFilter(_ sender: Any) {
        var query = LocationQueryParams()
        
        query.lat = lat
        query.lng = lng
        
        viewModel.requestSnapsbyLocation(query: query)
        
    }
    @IBAction func performSearch(_ sender: Any) {
        
        if(!countryText.text!.isEmpty){
            query.country = countryText.text
        }
        if(orderBy != nil){
            query.order = orderBy
        }
        if(sortBy != nil){
            query.sort = sortBy
        }
        if(OffSetSlider.value > 0){
            query.offset = Int(OffSetSlider.value)
        }
        if(LimitSlider.value > 0){
            query.limit = Int(LimitSlider.value)
        }
        
        viewModel.requestSnaps(query: query)
    }
    /*
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        if identifier == "bla" {
            //let trenzinho = sender as? User
            //viewController.viewModel.tremDoOutroTrem
        }
    }
 */
}

extension ViewController: RequestViewModelDelegate{
    func snapLoaded() {
        if(viewModel.getSizeOfSnapsResults() == 0) {
            noResultText.isHidden = false
        } else {
            noResultText.isHidden = true
        }
        tableViewSnaps.reloadData()
    }
}

extension ViewController: UITableViewDelegate {
   /* func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "bla", sender: nil)
    }
 */
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //viewmodel.arrayTrensimnho.count ? 0
        return viewModel.getSizeOfSnapsResults()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SnapsTableViewCell = self.tableViewSnaps.dequeueReusableCell(withIdentifier: "snapsTableViewCell") as! SnapsTableViewCell
        cell.bind(result: viewModel.getResultAtPoisition(index: indexPath.row)!)
        return cell
    }
    
}

extension ViewController: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
}
