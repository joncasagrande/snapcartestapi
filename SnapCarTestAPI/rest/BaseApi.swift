//
//  BaseApi.swift
//  SnapCarTestAPI
//
//  Created by Jonathan on 29/08/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
//


import Foundation
import Alamofire

enum BaseApi : URLRequestConvertible{
    
    
    static let urlRequest :String = "https://api.snappcar.nl/v2/search"
    
    case search(params : QueryParams)
    case locationSearch(params: LocationQueryParams)
    
    func asURLRequest() throws -> URLRequest {
        let url = try BaseApi.urlRequest.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent("/query"))
            urlRequest.httpMethod = HTTPMethod.get.rawValue
        
        switch self {
        case .search(let parameters):
            urlRequest = try URLEncodedFormParameterEncoder.default.encode(parameters, into: urlRequest)
        case .locationSearch(let parameters):
            urlRequest = try URLEncodedFormParameterEncoder.default.encode(parameters, into: urlRequest)
        }
        
        return urlRequest
    }
    
    
}
