//
//  UserTest.swift
//  SnapCarTestAPITests
//
//  Created by Jonathan on 31/08/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
//

import Foundation
import XCTest

@testable import SnapCarTestAPI

class UserTests: XCTestCase {
    var user : User?
    let jsonUser = Data("""
        {
                "firstName": "Floris",
                "imageUrl": "https://cdn.snappcar.nl/images/adba14d4-b666-40a8-8781-bb68063d88f3/c0c963ca-b1b0-417f-8ba9-a2c5f77d05f4",
                "street": "Herenstraat",
                "city": "Utrecht"
            }
""".utf8)
    
    override func setUp() {
        user = try? JSONDecoder().decode(User.self, from: jsonUser)
    }
    
    override func tearDown() {
       // user = nil
    }
    
    func testParseJson() {
        XCTAssertNotNil(user)
    }
    
    func didChange() {
        XCTAssertEqual(user?.firstName, "Floris")
    }
    
}
