//
//  QueryParams.swift
//  SnapCarTestAPI
//
//  Created by Jonathan on 29/08/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
//
/*
 country    String    n/a    NL, DE, DK, SE    Only show results for a specific country
 sort    String    n/a    recommended, price, distance    Sort the results by the specified type
 order    String    asc    asc, desc    Sort the results in the specified direction
 limit    Integer    10    1-10    Limit the number of results
 offset
 */

import Foundation

struct QueryParams : Encodable{
    var country : String?
    var sort: String?
    var order: String?
    var limit : Int?
    var offset: Int?
    
}
