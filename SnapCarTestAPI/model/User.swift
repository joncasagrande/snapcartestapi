//
//  User.swift
//  SnapCarTestAPI
//
//  Created by Jonathan on 28/08/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
/*
 "user": {
 "firstName": "Floris",
 "imageUrl": "https://cdn.snappcar.nl/images/adba14d4-b666-40a8-8781-bb68063d88f3/c0c963ca-b1b0-417f-8ba9-a2c5f77d05f4",
 "street": "Herenstraat",
 "city": "Utrecht"
 },
*/

import Foundation

struct User: Codable {
    let firstName: String?
    let imageURL: String?
    let street : String?
    let city: String?
    
    enum CodingKeys: String, CodingKey {
        case firstName = "firstName"
        case imageURL = "imageURL"
        case street = "street"
        case city = "city"
    }
}
