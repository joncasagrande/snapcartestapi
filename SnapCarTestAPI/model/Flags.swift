//
//  Flags.swift
//  SnapCarTestAPI
//
//  Created by Jonathan on 28/08/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
/*
 "flags": {
 "favorite": false,
 "new": false,
 "instantBookable": false,
 "previouslyRented": false,
 "isKeyless": false
 }
 */

import Foundation
struct Flags: Codable {
    let favorite, new, instantBookable, previouslyRented: Bool?
    let isKeyless: Bool?
}
