//
//  PriceInformation.swift
//  SnapCarTestAPI
//
//  Created by Jonathan on 28/08/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
/*
 "priceInformation": {
 "price": 30,
 "pricePerKilometer": 0.25,
 "freeKilometersPerDay": 100,
 "rentalDays": 1,
 "isoCurrencyCode": "EUR"
 }
 */

import Foundation
struct PriceInformation: Codable {
    let price: Double?
    let pricePerKilometer: Double?
    let freeKilometersPerDay, rentalDays: Int?
    let isoCurrencyCode: String?
}
