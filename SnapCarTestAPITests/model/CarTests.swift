//
//  CarTests.swift
//  SnapCarTestAPITests
//
//  Created by Jonathan on 31/08/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
//

import Foundation
import XCTest

@testable import SnapCarTestAPI

class CarTests: XCTestCase {
    var car : Car?
    let jsonCar = Data("""
        {
            "reviewAvg": 5,
            "fuelType": "Petrol",
            "createdAt": "2017-03-23T11:06:48.747Z",
            "ownerId": "adba14d4-b666-40a8-8781-bb68063d88f3",
            "year": 2017,
            "reviewCount": 14,
            "make": "Fiat",
            "gear": "Manual",
            "bodyType": "hatchback",
            "model": 500,
            "seats": 4,
            "allowed": [
            "kids"
            ],
            "accessories": [
            "airco",
            "iPod",
            "radio"
            ],
            "images": [
            "https://dzklgi3s0q69j.cloudfront.net/image.aspx?ii=f37b0eae-3b56-4ec7-beaa-fff3c53ea766",
            "https://dzklgi3s0q69j.cloudfront.net/image.aspx?ii=10618bc4-6093-49fa-b941-464268a627e6",
            "https://dzklgi3s0q69j.cloudfront.net/image.aspx?ii=bdacab9d-6cb9-484a-90bd-96f82a05a3c7",
            "https://dzklgi3s0q69j.cloudfront.net/image.aspx?ii=c2239e54-4bb8-46c6-a063-06ee2bd48542",
            "https://dzklgi3s0q69j.cloudfront.net/image.aspx?ii=eb2c870a-1722-4bf3-9433-6f0de2558012",
            "https://dzklgi3s0q69j.cloudfront.net/image.aspx?ii=d88abd7a-6366-4fab-bbfb-c5f7b98ec7e2"
            ]
    }
""".utf8)

    override func setUp() {
        car = try? JSONDecoder().decode(Car.self, from: jsonCar)
    }
    
    override func tearDown() {
        car = nil
    }
    
    func testParseJson() {
        XCTAssertNotNil(car)
        
    }
    func testReview() {
        XCTAssertTrue(car?.reviewAvg == 5)
    }
    
}
