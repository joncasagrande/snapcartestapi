//
//  RequestViewModel.swift
//  SnapCarTestAPI
//
//  Created by Jonathan on 29/08/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
//

import Foundation
import Alamofire

protocol RequestViewModelDelegate {
    func snapLoaded()
}

class RequestViewModel{
    
    private var snaps : Snaps?
    var delegate : RequestViewModelDelegate?
    
    func requestSnaps(query : QueryParams) {
        let req = AF.request(BaseApi.search(params: query))
        requestResult(req)
    }
    
    fileprivate func requestResult(_ req: DataRequest) {
        req.responseDecodable { (res: DataResponse<Snaps>) in
            if let error = res.error {
                debugPrint(error.localizedDescription)
            } else if let snappReqest = res.value {
                self.snaps = snappReqest
                self.delegate?.snapLoaded()
            }
        }
    }
    
    func requestSnapsbyLocation(query : LocationQueryParams) {
        let req = AF.request(BaseApi.locationSearch(params: query))
        requestResult(req)
        print(req.description)
    }
    
    
    func getSizeOfSnapsResults() -> Int {
        if(snaps?.results == nil){
            return 0
        }
        
        return snaps?.results!.count ?? 0 
    }
    
    func getResultAtPoisition(index: Int) -> Result? {
        if(snaps == nil) {
            return nil
        }
        return snaps?.results?[index]
    }

    
    func newJSONDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
            decoder.dateDecodingStrategy = .iso8601
        }
        return decoder
    }
    
    func newJSONEncoder() -> JSONEncoder {
        let encoder = JSONEncoder()
        if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
            encoder.dateEncodingStrategy = .iso8601
        }
        return encoder
    }
    
}
