//
//  LocationQueryParams.swift
//  SnapCarTestAPI
//
//  Created by Jonathan on 29/08/2019.
//  Copyright © 2019 Jonathan. All rights reserved.
//

import Foundation

struct LocationQueryParams : Encodable {
    var lat: Double?
    var lng: Double?
    var maxDistance: Int?
}
